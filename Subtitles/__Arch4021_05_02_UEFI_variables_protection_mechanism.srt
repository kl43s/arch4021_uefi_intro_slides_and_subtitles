1
00:00:00,299 --> 00:00:05,640
But what protection machines exist to

2
00:00:02,879 --> 00:00:08,160
defend our UEFI variables. There are

3
00:00:05,640 --> 00:00:11,219
multiple protection mechanisms which we

4
00:00:08,160 --> 00:00:13,940
can mainly divide into two section pair

5
00:00:11,219 --> 00:00:17,699
attack resistance, there are software

6
00:00:13,940 --> 00:00:20,160
attack resistance protection mechanism

7
00:00:17,699 --> 00:00:22,980
and hardware attack resistance

8
00:00:20,160 --> 00:00:25,439
protection mechanism. We would like to

9
00:00:22,980 --> 00:00:28,619
not talk too much about hardware

10
00:00:25,439 --> 00:00:32,040
attack protection mechanism just to

11
00:00:28,619 --> 00:00:34,340
mention a couple of on this slide. Software

12
00:00:32,040 --> 00:00:37,739
protection mechanisms will be exploited

13
00:00:34,340 --> 00:00:38,700
more widely in the further part of the

14
00:00:37,739 --> 00:00:41,100
course.

15
00:00:38,700 --> 00:00:44,640
So, let's start with hardware integrity

16
00:00:41,100 --> 00:00:46,920
protection mechanism. First is [unintelligible]

17
00:00:44,640 --> 00:00:49,200
protect memory block partition which is

18
00:00:46,920 --> 00:00:52,320
supported by some hardware devices like

19
00:00:49,200 --> 00:00:56,820
NVMe universal flash storage

20
00:00:52,320 --> 00:00:59,280
or EMMC, so this RPMB is typically used

21
00:00:56,820 --> 00:01:02,399
in combination with trusted execution

22
00:00:59,280 --> 00:01:04,979
environment. It works in a way that

23
00:01:02,399 --> 00:01:09,380
during the manufacturing process of the

24
00:01:04,979 --> 00:01:11,939
storage there is RPMB key generated

25
00:01:09,380 --> 00:01:15,420
which is fused into one-time

26
00:01:11,939 --> 00:01:18,240
programmable fuses, and this key can be

27
00:01:15,420 --> 00:01:22,140
then used to access data that are

28
00:01:18,240 --> 00:01:24,119
already in RPMB partition, and this key

29
00:01:22,140 --> 00:01:26,820
can be used only from the trusted

30
00:01:24,119 --> 00:01:31,140
execution environment to even improve

31
00:01:26,820 --> 00:01:33,540
the security of RPMB, there is also a

32
00:01:31,140 --> 00:01:38,100
reply protected monotonic counter which

33
00:01:33,540 --> 00:01:42,420
also has its own key fused in OTP and it

34
00:01:38,100 --> 00:01:44,280
is used to avoid reply attacks. Other

35
00:01:42,420 --> 00:01:46,500
means which can be used to protect

36
00:01:44,280 --> 00:01:48,299
against hardware attacks and like

37
00:01:46,500 --> 00:01:51,060
protecting our UEFI variables against

38
00:01:48,299 --> 00:01:55,200
hardware attacks is storing those UEFI

39
00:01:51,060 --> 00:01:59,280
variables in TPM, CSME and in hardware

40
00:01:55,200 --> 00:02:01,979
security module or

41
00:01:59,280 --> 00:02:04,560
other type of secure storage. So,

42
00:02:01,979 --> 00:02:06,600
interestingly UEFI designers in the

43
00:02:04,560 --> 00:02:08,940
specification considered such use case

44
00:02:06,600 --> 00:02:12,360
and we will talk about that later

45
00:02:08,940 --> 00:02:14,280
despite those elements in real life.

46
00:02:12,360 --> 00:02:16,080
From the 

47
00:02:14,280 --> 00:02:19,280
hardware availability protection

48
00:02:16,080 --> 00:02:22,319
mechanism we have two here. First is

49
00:02:19,280 --> 00:02:24,860
atomicity. It is guaranteed by the

50
00:02:22,319 --> 00:02:28,560
hardware, because nor flash designs

51
00:02:24,860 --> 00:02:31,980
guarantee atomicity of writing one

52
00:02:28,560 --> 00:02:34,200
bite, but of course to write whole UEfI

53
00:02:31,980 --> 00:02:37,200
variable we need the implementation of

54
00:02:34,200 --> 00:02:40,020
correct update flow, which is already

55
00:02:37,200 --> 00:02:43,920
part of EDK2 implementation. So it kind

56
00:02:40,020 --> 00:02:46,200
of works for us in background by

57
00:02:43,920 --> 00:02:49,440
default. There is also four tolerant

58
00:02:46,200 --> 00:02:52,920
right, so it may happen that

59
00:02:49,440 --> 00:02:56,340
attacker might try to do power loss in

60
00:02:52,920 --> 00:03:00,060
the middle of the UEFI variable update

61
00:02:56,340 --> 00:03:03,060
to break our security and it is already

62
00:03:00,060 --> 00:03:06,000
implemented also in EDK2 as a driver but

63
00:03:03,060 --> 00:03:10,200
it's a separate driver which

64
00:03:06,000 --> 00:03:13,200
have to be enabled to work in

65
00:03:10,200 --> 00:03:15,720
default compilation. 

66
00:03:13,200 --> 00:03:18,980
what this driver really does

67
00:03:15,720 --> 00:03:23,540
is just tracking precisely

68
00:03:18,980 --> 00:03:26,760
what right transaction sent to our

69
00:03:23,540 --> 00:03:30,720
SPI or to other storage that stores UEFI

70
00:03:26,760 --> 00:03:33,840
variables are completed and only based

71
00:03:30,720 --> 00:03:36,480
on that decide if the whole variable

72
00:03:33,840 --> 00:03:39,420
was written or not and then if something

73
00:03:36,480 --> 00:03:42,540
happened in the middle, we are sure that

74
00:03:39,420 --> 00:03:45,780
not whole variable was written to the

75
00:03:42,540 --> 00:03:48,360
storage. And last technique is used for

76
00:03:45,780 --> 00:03:51,480
confidentiality protection for

77
00:03:48,360 --> 00:03:53,480
hardware confidentiality protection

78
00:03:51,480 --> 00:03:58,140
this technique helps

79
00:03:53,480 --> 00:04:00,560
helps us not only storing the

80
00:03:58,140 --> 00:04:03,480
variables in case of

81
00:04:00,560 --> 00:04:07,799
integrity protection, not only storing in

82
00:04:03,480 --> 00:04:11,580
TPM, CSME or other protected storage,

83
00:04:07,799 --> 00:04:13,500
but storing encrypted version of the UEFI

84
00:04:11,580 --> 00:04:17,160
variables. So the question is how we

85
00:04:13,500 --> 00:04:19,739
decrypt those encrypted variables? We can

86
00:04:17,160 --> 00:04:21,900
decrypt it either by user means which

87
00:04:19,739 --> 00:04:24,840
means providing password during boot or

88
00:04:21,900 --> 00:04:28,020
some fingerprint or some USB token, or by

89
00:04:24,840 --> 00:04:32,220
platform means which can be for example

90
00:04:28,020 --> 00:04:35,360
a secret that is unsealed from the TPM

91
00:04:32,220 --> 00:04:38,100
after PCRs in TPM will be correctly

92
00:04:35,360 --> 00:04:40,740
populated. And that's it from

93
00:04:38,100 --> 00:04:44,340
hardware perspective and now we can talk

94
00:04:40,740 --> 00:04:47,120
about resisting software attacks and

95
00:04:44,340 --> 00:04:49,979
protecting integrity availability and

96
00:04:47,120 --> 00:04:51,419
confidentiality using software attack

97
00:04:49,979 --> 00:04:54,840
resistance.

98
00:04:51,419 --> 00:04:57,960
So, let's start with mechanisms that can

99
00:04:54,840 --> 00:05:01,380
help us resist software attacks on

100
00:04:57,960 --> 00:05:03,840
UEFI variables and and let's start with

101
00:05:01,380 --> 00:05:06,720
confidentiality protection, we already

102
00:05:03,840 --> 00:05:10,080
discussed that as a part of the hardware

103
00:05:06,720 --> 00:05:12,960
part of the protection but the point

104
00:05:10,080 --> 00:05:16,320
here is that also the software part so

105
00:05:12,960 --> 00:05:19,940
the way that user provides the password

106
00:05:16,320 --> 00:05:22,320
or the content for generating key

107
00:05:19,940 --> 00:05:26,100
also have to be protected and of course

108
00:05:22,320 --> 00:05:28,680
that's up to the implementer in

109
00:05:26,100 --> 00:05:31,080
the UEFI. Then let's jump to the

110
00:05:28,680 --> 00:05:34,380
availability protection because

111
00:05:31,080 --> 00:05:37,979
availability protection is built in EDK2

112
00:05:34,380 --> 00:05:40,919
and we can say it's technology just

113
00:05:37,979 --> 00:05:43,800
would just work in in background. We can

114
00:05:40,919 --> 00:05:46,979
say that this can be safely omitted

115
00:05:43,800 --> 00:05:49,560
and we will not talk much about that, of

116
00:05:46,979 --> 00:05:52,440
course both flashware out protection as

117
00:05:49,560 --> 00:05:54,720
well as quota management implementations

118
00:05:52,440 --> 00:05:56,900
could be interesting areas for

119
00:05:54,720 --> 00:06:00,720
researchers looking for vulnerabilities,

120
00:05:56,900 --> 00:06:03,120
but those are not meant to be used by

121
00:06:00,720 --> 00:06:05,520
firmware developers in kind of

122
00:06:03,120 --> 00:06:07,740
regular everyday development workflow

123
00:06:05,520 --> 00:06:09,240
and we think that Integrity

124
00:06:07,740 --> 00:06:12,120
protection and variable

125
00:06:09,240 --> 00:06:16,199
authentication is way more important

126
00:06:12,120 --> 00:06:19,919
and we often use it. So let's go to

127
00:06:16,199 --> 00:06:22,500
Integrity protection mechanism as the

128
00:06:19,919 --> 00:06:25,560
means of defending against software

129
00:06:22,500 --> 00:06:28,560
attacks. So, main and most used and most

130
00:06:25,560 --> 00:06:31,199
popular and the one which will be

131
00:06:28,560 --> 00:06:34,080
widely discussed in this course is

132
00:06:31,199 --> 00:06:37,020
variable authentication this mechanism is

133
00:06:34,080 --> 00:06:40,620
is for ensuring that the entity which calls

134
00:06:37,020 --> 00:06:43,860
UEFI runtime services function set

135
00:06:40,620 --> 00:06:46,460
variables which is used for setting

136
00:06:43,860 --> 00:06:49,699
and updating the content of the variable

137
00:06:46,460 --> 00:06:52,440
has the authority to really do that

138
00:06:49,699 --> 00:06:55,620
operation. There are various

139
00:06:52,440 --> 00:06:58,080
authentication mechanisms to prove that

140
00:06:55,620 --> 00:07:01,319
and we will discuss those authentication

141
00:06:58,080 --> 00:07:03,360
machines on the next slides.

142
00:07:01,319 --> 00:07:06,680
The second Integrity protection

143
00:07:03,360 --> 00:07:08,880
mechanism is accessing and updating

144
00:07:06,680 --> 00:07:12,300
variables through trusted execution

145
00:07:08,880 --> 00:07:14,699
environment which in case of x86 is a

146
00:07:12,300 --> 00:07:18,539
system management mode and it

147
00:07:14,699 --> 00:07:21,660
works in a way and the belief is that if

148
00:07:18,539 --> 00:07:24,560
we do the access to the variables in

149
00:07:21,660 --> 00:07:27,419
tightly controlled and isolated

150
00:07:24,560 --> 00:07:29,940
environment it improves protection, of

151
00:07:27,419 --> 00:07:32,280
course this method is fine as long as

152
00:07:29,940 --> 00:07:35,699
we like a system management mode and

153
00:07:32,280 --> 00:07:37,620
we trust in in system management mode

154
00:07:35,699 --> 00:07:40,440
implementation. There are some

155
00:07:37,620 --> 00:07:43,860
platforms which does not support us smm

156
00:07:40,440 --> 00:07:47,280
and there are some designs and thread

157
00:07:43,860 --> 00:07:51,479
models we just don't want smm to

158
00:07:47,280 --> 00:07:53,880
be part of the platform.

159
00:07:51,479 --> 00:07:56,340
So, in such case implementation which is

160
00:07:53,880 --> 00:07:58,560
already done in EDK2 and is default

161
00:07:56,340 --> 00:07:59,220
implementation to handle

162
00:07:58,560 --> 00:08:03,780


163
00:07:59,220 --> 00:08:06,180
UEFI variables updates cannot be used. So

164
00:08:03,780 --> 00:08:10,380
one have to implement his own. Other

165
00:08:06,180 --> 00:08:13,620
mechanism is log or variable log it may

166
00:08:10,380 --> 00:08:16,560
happen that some variables should not be

167
00:08:13,620 --> 00:08:19,340
changed after some boot stage. So

168
00:08:16,560 --> 00:08:22,500
typically, those variables are like

169
00:08:19,340 --> 00:08:25,440
configuration related to memory training

170
00:08:22,500 --> 00:08:29,879
or for example some stuff related to

171
00:08:25,440 --> 00:08:32,880
system management mode memory and those

172
00:08:29,879 --> 00:08:35,940
configuration options or configuration

173
00:08:32,880 --> 00:08:38,940
parameters can be locked and after those

174
00:08:35,940 --> 00:08:42,240
will be locked they cannot be changed

175
00:08:38,940 --> 00:08:44,580
and until reboot. Finally, there are also

176
00:08:42,240 --> 00:08:47,880
sanity checks, sanity checks help make

177
00:08:44,580 --> 00:08:50,040
sure that variable can get only valid

178
00:08:47,880 --> 00:08:52,260
value typically this is done in BIOS

179
00:08:50,040 --> 00:08:56,220
setup menu when we go to setup menu and

180
00:08:52,260 --> 00:09:00,959
want to set given value-give some data

181
00:08:56,220 --> 00:09:03,420
to given variable and then BIOS

182
00:09:00,959 --> 00:09:05,640
setup menu validates if this is a

183
00:09:03,420 --> 00:09:07,800
correct value, if this is in range if

184
00:09:05,640 --> 00:09:12,060
this is not below minimum or over

185
00:09:07,800 --> 00:09:14,760
maximum and so on, if it is then it

186
00:09:12,060 --> 00:09:17,459
does not allow us update the variable in

187
00:09:14,760 --> 00:09:19,800
such way. And in this in this course we

188
00:09:17,459 --> 00:09:22,800
mostly focusing as I said on variable

189
00:09:19,800 --> 00:09:25,560
authentication and we will also talk a

190
00:09:22,800 --> 00:09:28,700
little bit about updating variables in

191
00:09:25,560 --> 00:09:28,700
trusted execution environment.


