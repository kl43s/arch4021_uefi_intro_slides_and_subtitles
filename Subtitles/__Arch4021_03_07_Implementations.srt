1
00:00:00,640 --> 00:00:05,359
What are the implementations of the

2
00:00:03,679 --> 00:00:07,120
of the UEFI.

3
00:00:05,359 --> 00:00:10,240
So,

4
00:00:07,120 --> 00:00:11,759
UEFI itself is a just a

5
00:00:10,240 --> 00:00:14,080
specification

6
00:00:11,759 --> 00:00:15,120
and it has multiple implementations

7
00:00:14,080 --> 00:00:18,640
although

8
00:00:15,120 --> 00:00:19,840
not many are very popular or maybe

9
00:00:18,640 --> 00:00:22,560
not many

10
00:00:19,840 --> 00:00:24,800
are open source and popular and the most

11
00:00:22,560 --> 00:00:28,160
popular is EDK2 which is reference

12
00:00:24,800 --> 00:00:30,880
implementation and is usually a base for

13
00:00:28,160 --> 00:00:34,000
any other custom implementation

14
00:00:30,880 --> 00:00:36,800
and even the proprietary one.

15
00:00:34,000 --> 00:00:39,520
EDK2 can build from

16
00:00:36,800 --> 00:00:41,200
for many targets there is a QEMU target

17
00:00:39,520 --> 00:00:42,079
which

18
00:00:41,200 --> 00:00:44,960


19
00:00:42,079 --> 00:00:47,200
gives us ability to

20
00:00:44,960 --> 00:00:48,960
use it in the virtual machine

21
00:00:47,200 --> 00:00:51,280
environment so

22
00:00:48,960 --> 00:00:53,440
the QEMU target is

23
00:00:51,280 --> 00:00:56,160
is called open virtual machine firmware

24
00:00:53,440 --> 00:00:57,870
OVMF, and we will use it during

25
00:00:56,160 --> 00:00:59,680
this course.

26
00:00:57,870 --> 00:01:01,359


27
00:00:59,680 --> 00:01:05,199
But

28
00:01:01,359 --> 00:01:07,119
in case of building usable

29
00:01:05,199 --> 00:01:10,000
firmware from EDK2 we need some

30
00:01:07,119 --> 00:01:13,439
additional firmware components sometimes

31
00:01:10,000 --> 00:01:15,680
even binary objects from the silicon

32
00:01:13,439 --> 00:01:17,600
vendor. There are also some there are

33
00:01:15,680 --> 00:01:22,080
also some commercial implementations

34
00:01:17,600 --> 00:01:23,759
like InsydeH2O or

35
00:01:22,080 --> 00:01:26,799
AMI Aptio

36
00:01:23,759 --> 00:01:28,479
but those require additional

37
00:01:26,799 --> 00:01:31,280
licenses and

38
00:01:28,479 --> 00:01:33,600
agreements with those vendors

39
00:01:31,280 --> 00:01:36,159
it is also possible to build

40
00:01:33,600 --> 00:01:37,520
UEFI compatible firmware by combining

41
00:01:36,159 --> 00:01:40,000
coreboot and

42
00:01:37,520 --> 00:01:42,720
some part of EDK2 which is called TianoCore

43
00:01:40,000 --> 00:01:43,920
payload and we will talk about

44
00:01:42,720 --> 00:01:46,960
that also

45
00:01:43,920 --> 00:01:48,240
later

46
00:01:46,960 --> 00:01:49,759
in this lecture.

47
00:01:48,240 --> 00:01:52,640
There is also

48
00:01:49,759 --> 00:01:53,600
an implementation called SlimBootloader

49
00:01:52,640 --> 00:01:56,079


50
00:01:53,600 --> 00:01:59,360
there are some claims that this is

51
00:01:56,079 --> 00:02:00,479
an open source boot firmware which is

52
00:01:59,360 --> 00:02:02,759
something

53
00:02:00,479 --> 00:02:05,439
different and

54
00:02:02,759 --> 00:02:08,160
distinguishable from EDK2 but

55
00:02:05,439 --> 00:02:10,959
essentially it use minimal subset of

56
00:02:08,160 --> 00:02:11,760
EDK2 to create very

57
00:02:10,959 --> 00:02:14,000


58
00:02:11,760 --> 00:02:15,200
simple very fast booting

59
00:02:14,000 --> 00:02:19,120


60
00:02:15,200 --> 00:02:21,040
minimal firmware.

61
00:02:19,120 --> 00:02:23,840
And the tooling is the same

62
00:02:21,040 --> 00:02:25,599
as in in EDK2 and the code base is

63
00:02:23,840 --> 00:02:28,640
exactly the same the syntax of the code

64
00:02:25,599 --> 00:02:31,920
base is exactly the same. So this is

65
00:02:28,640 --> 00:02:35,040
essentially smaller version of the EDK2.

66
00:02:31,920 --> 00:02:37,840
But with support for 

67
00:02:35,040 --> 00:02:38,640
for already some target platforms so

68
00:02:37,840 --> 00:02:40,000
like

69
00:02:38,640 --> 00:02:43,519
Intel

70
00:02:40,000 --> 00:02:43,519
customer reference boards.

71
00:02:44,080 --> 00:02:48,480
What are their other

72
00:02:46,879 --> 00:02:50,720
implementations so there is a

73
00:02:48,480 --> 00:02:52,160
implementation called Yabits

74
00:02:50,720 --> 00:02:54,160
yet another

75
00:02:52,160 --> 00:02:57,760
UEFI implementation

76
00:02:54,160 --> 00:03:01,920
and this is Minoca OS based minimal UEFI

77
00:02:57,760 --> 00:03:03,680
implementation which you can find on the

78
00:03:01,920 --> 00:03:05,920
github.

79
00:03:03,680 --> 00:03:09,360
It can be used as a coreboot payload in

80
00:03:05,920 --> 00:03:11,760
a similar fashion as TianoCore payload.

81
00:03:09,360 --> 00:03:12,879
There is implementation made by

82
00:03:11,760 --> 00:03:14,000
U-Boot

83
00:03:12,879 --> 00:03:16,480
team

84
00:03:14,000 --> 00:03:18,879
with the goal of complying to ARM

85
00:03:16,480 --> 00:03:20,239
embedded base boot requirements

86
00:03:18,879 --> 00:03:21,760
and

87
00:03:20,239 --> 00:03:25,920
making

88
00:03:21,760 --> 00:03:27,120
U-Boot UEFI compatible and entering

89
00:03:25,920 --> 00:03:30,640
U-Boot

90
00:03:27,120 --> 00:03:33,840
into advanced embedded

91
00:03:30,640 --> 00:03:33,840
certified world,

92
00:03:33,920 --> 00:03:38,000
and probably server world also.

93
00:03:38,080 --> 00:03:41,840
So, part of the specification was

94
00:03:39,920 --> 00:03:43,120
implemented and this is documented in

95
00:03:41,840 --> 00:03:44,400
the U-Boot

96
00:03:43,120 --> 00:03:46,799
documentation.

97
00:03:44,400 --> 00:03:48,480
There is u-root which is go user

98
00:03:46,799 --> 00:03:50,720
space with

99
00:03:48,480 --> 00:03:53,840
Linux bootloaders it is used by

100
00:03:50,720 --> 00:03:56,799
LinuxBoot community, LinuxBoot is

101
00:03:53,840 --> 00:03:58,799
just Linux + u-root + kernel +

102
00:03:56,799 --> 00:04:02,239
user space,

103
00:03:58,799 --> 00:04:04,239
and it implements some UEFI

104
00:04:02,239 --> 00:04:07,280
features to boot 

105
00:04:04,239 --> 00:04:10,799
UEFI compatible operating systems.

106
00:04:07,280 --> 00:04:13,599
There is a CloverBootloader which is

107
00:04:10,799 --> 00:04:15,120
for Mac OS Windows

108
00:04:13,599 --> 00:04:18,239
and Linux

109
00:04:15,120 --> 00:04:22,000
and also it reimplements partially

110
00:04:18,239 --> 00:04:26,240
some of the UEFI specification for

111
00:04:22,000 --> 00:04:26,240
the operating system boot needs.



