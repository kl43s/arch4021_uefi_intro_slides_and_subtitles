1
00:00:00,160 --> 00:00:04,240
UEFI specification also

2
00:00:02,399 --> 00:00:05,040
defines

3
00:00:04,240 --> 00:00:08,080
what

4
00:00:05,040 --> 00:00:09,599
features and what

5
00:00:08,080 --> 00:00:13,280


6
00:00:09,599 --> 00:00:17,119
what functions, and what API is supported

7
00:00:13,280 --> 00:00:18,320
in some core functions or core

8
00:00:17,119 --> 00:00:21,520
services.

9
00:00:18,320 --> 00:00:23,840
And this firmware core consists of two

10
00:00:21,520 --> 00:00:26,640
services, boot services and runtime

11
00:00:23,840 --> 00:00:27,439
services, boot services provide

12
00:00:26,640 --> 00:00:30,400


13
00:00:27,439 --> 00:00:32,480
event management so asynchronous

14
00:00:30,400 --> 00:00:34,719
trigger for various

15
00:00:32,480 --> 00:00:35,680
code execution,

16
00:00:34,719 --> 00:00:38,399
time

17
00:00:35,680 --> 00:00:40,320
functions, getting time setting

18
00:00:38,399 --> 00:00:43,200
time function and task priority

19
00:00:40,320 --> 00:00:46,320
functions, memory location,

20
00:00:43,200 --> 00:00:49,520
protocol handling which is protocols are

21
00:00:46,320 --> 00:00:52,719
API which is installed on given

22
00:00:49,520 --> 00:00:55,120
handle which the protocols will be

23
00:00:52,719 --> 00:00:56,079
described later in this lecture

24
00:00:55,120 --> 00:00:59,680


25
00:00:56,079 --> 00:01:02,640
in further section or subsection.

26
00:00:59,680 --> 00:01:03,600
Image services which means

27
00:01:02,640 --> 00:01:06,400


28
00:01:03,600 --> 00:01:09,439
services that helps look for

29
00:01:06,400 --> 00:01:11,360
and load and execute correct

30
00:01:09,439 --> 00:01:14,320
images and types of images we know, we

31
00:01:11,360 --> 00:01:17,040
know that there are UEFI images there are

32
00:01:14,320 --> 00:01:19,439
applications, OS loaders, drivers.

33
00:01:17,040 --> 00:01:21,520
There are some miscellaneous

34
00:01:19,439 --> 00:01:25,280


35
00:01:21,520 --> 00:01:26,479
services related to watchdog, memory set,

36
00:01:25,280 --> 00:01:30,400
copy

37
00:01:26,479 --> 00:01:32,560
some stalling, monotonic counter, some

38
00:01:30,400 --> 00:01:35,119
hashing or

39
00:01:32,560 --> 00:01:37,759
checksums.

40
00:01:35,119 --> 00:01:41,280
In runtime services typically

41
00:01:37,759 --> 00:01:44,560
we need access to a variable

42
00:01:41,280 --> 00:01:46,560
to change boot order to change some

43
00:01:44,560 --> 00:01:49,040
firmware settings,

44
00:01:46,560 --> 00:01:50,000
and that's why we have variable services

45
00:01:49,040 --> 00:01:52,960
which

46
00:01:50,000 --> 00:01:54,640
basically give get and set variables.

47
00:01:52,960 --> 00:01:58,799
There are some time services which may

48
00:01:54,640 --> 00:02:00,560
be needed to obtain trustworthy

49
00:01:58,799 --> 00:02:01,759
time

50
00:02:00,560 --> 00:02:04,320
from

51
00:02:01,759 --> 00:02:06,000
firmware or from some

52
00:02:04,320 --> 00:02:07,439
dedicated hardware

53
00:02:06,000 --> 00:02:08,800
and there are some virtual memory

54
00:02:07,439 --> 00:02:10,000
services

55
00:02:08,800 --> 00:02:12,400
which deal

56
00:02:10,000 --> 00:02:13,920
with conversion between physical and

57
00:02:12,400 --> 00:02:16,239
virtual addressing

58
00:02:13,920 --> 00:02:18,720
and some additional

59
00:02:16,239 --> 00:02:19,760
services which may help in firmware

60
00:02:18,720 --> 00:02:22,239
update,

61
00:02:19,760 --> 00:02:25,280
resetting the platform or getting

62
00:02:22,239 --> 00:02:25,280
monotonic counter.



